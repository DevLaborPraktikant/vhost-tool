<?php

/*
 * vhost Tool
 * Author: Norman Huth & Adrian Jakob
 * https://devlabor.com/
 *
 * */

CONST HOST_FILE      = 'C:\Windows\System32\drivers\etc\hosts';
CONST CONF_FILE      = 'C:\xampp\apache\conf\extra\httpd-vhosts.conf';
CONST JSON_FILE      = 'vhosts.json';
CONST APACHE_RESTART = 'C:\xampp\apache_start.bat';

//die(json_encode([
//        'localhost' => 'C:\\xampp\\htdocs\\1',
//        'localhost2' => 'C:\\xampp\\htdocs\\2',
//        'localhost3' => 'C:\\xampp\\htdocs\\3',
//]));
$file  = file_exists(JSON_FILE) ? file_get_contents(JSON_FILE):'';
$hosts = $file ? json_decode($file) : [];

if(isset($_POST['host']) && isset($_POST['folder'])) {
	asort($_POST['host']);
    $vhosts=[];
    $conf_file=$hosts_file='';
    foreach ($_POST['host'] as $i => $host) {
        if($host!='' && isset($_POST['folder'][$i]) && $_POST['folder'][$i]!='') {
	        $array = [
		        trim($host) => str_replace('\\','/',trim($_POST['folder'][$i]))
            ];
	        $vhosts = $vhosts+$array;

	        if($conf_file)
		        $conf_file.="\n\n";
	        $conf_file.='<VirtualHost *:80>
	ServerName '.trim($host).'
	DocumentRoot "'.trim($_POST['folder'][$i]).'"
	ErrorLog "logs/'.trim($host).'.log"
	CustomLog "logs/'.trim($host).'.log" common
	<Directory "'.trim($_POST['folder'][$i]).'/">
		Options Indexes FollowSymLinks
		AllowOverride All
		Require all granted
	</Directory>
</VirtualHost>
<VirtualHost *:443>
	ServerName '.trim($host).'
	DocumentRoot "'.trim($_POST['folder'][$i]).'"
	SSLEngine On
	SSLCertificateFile "C:/xampp/apache/conf/ssl.crt/server.crt"
	SSLCertificateKeyFile "C:/xampp/apache/conf/ssl.key/server.key"
	<Directory "'.trim($_POST['folder'][$i]).'">
		Options Indexes FollowSymLinks
		AllowOverride All
		Require all granted
	</Directory>
</VirtualHost>';
	        if($hosts_file)
		        $hosts_file.="\n";
	        $hosts_file.="127.0.0.1\t\t".trim($host);
        }
    }

    file_put_contents(JSON_FILE,json_encode($vhosts));
    file_put_contents(CONF_FILE,$conf_file);
    file_put_contents(HOST_FILE,$hosts_file);
	system('cmd /c C:'.__DIR__.'/apache_restart.bat');
    header('location: '.$_SERVER['REQUEST_URI']);
    exit;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>vhost Tool</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <form method="post" action="index.php">
        <div class="card mt-4">
            <div class="card-header text-center">
                <h1 class="h5">Your Virtual Hosts</h1>
            </div>
            <ul class="list-group list-group-flush">
			    <?php

                $i=0;
			    foreach ($hosts as $host => $folder) {
				    echo '<li class="list-group-item">
                            <div class="text-center">
                                <a href="http://'.htmlspecialchars(trim($host)).'" target="_blank">'.htmlspecialchars(trim($host)).'</a>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <input type="text" class="form-control form-control-sm" placeholder="Host" value="'.htmlspecialchars(trim($host)).'" name="host['.$i.']">
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control form-control-sm" placeholder="Folder" value="'.htmlspecialchars(str_replace('/','\\',trim($folder))).'" name="folder['.$i.']">
                                </div>
                            </div>
                        </li>';
				    $i++;
			    }
			    ?>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control form-control-sm" placeholder="Host" name="host[New]">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control form-control-sm" placeholder="Folder" name="folder[New]">
                        </div>
                    </div>
                </li>
            </ul>
            <div class="card-footer text-center">
                <button type="submit" class="btn btn-info">Save VHosts</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>